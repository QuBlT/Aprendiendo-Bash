#!/bin/bash

# Agenda,
# 0. sin argumentos muestra la agenda
#
# 1. lee por la linea de comandos un nombre
#
# 2. comprueba si existe
#
#   2.1 Si existe muestra la información
#   2.2 Si no existe 
#       2.2.1 Pregunta si lo quiere añadir
#       2.2.2 Lo añade al final del fichero 
#       2.2.3 Lo ordena

function checkString {

    if [[ "$1" =~ ^[a-zA-Z]*$ ]]; then
        echo "Cadena Valida"
        return 0
    else
        echo 
        echo "${1} No es valido introducir solo letras de A - Z a - z"
        return 1  
    fi
}


function checkNum {

    if [[ "$1" =~ [0-9]{9} ]]; then
        echo "Numero valido"
        return 0
    else
        echo 
        echo "${1} No es valido introducir 9 numeros"
        return 1  
    fi
}

if [ -z $1 ]; then
    # Muestra la agenda actual si no se introducen argumentos
    
    echo "***********************************************************************"
    echo "*                           AGENDA 0.0                                *"
    echo "***********************************************************************"
    cat agenda.txt
    exit 0

else

    nombre=$1
    buscar=$(grep ${nombre} "agenda.txt")

    if [ "$?" = 0 ]; then
    #if [ "$nombre" = "$buscar" ]; then
        # si existe muestra el resultado de la list y sale
        echo "$buscar"
        echo 
        
        echo "¿Borrarlo de la lista? (s/n)"
        read respuesta
        
        if [[ "$respuesta" = [sS]* ]]; then
            sed -i "/$nombre/d" agenda.txt
            echo
            echo "Contacto ${nombre} borrado de la agenda"
            exit 0
        else
            exit 0
        fi
    else
        # Si la busqueda no devuelve nada PREGUNTAR SI QUIERE AGREGARLO

        echo "No existe el contacto ${nombre}, ¿Añadir un contacto nuevo? (s/n)"
        read respuesta
        
        if [[ "$respuesta" = [sS]* ]]; then
            echo -n "Introduce apellido: "
            read apellido

            checkString $apellido

            echo -n "Introduce numero: "
            read tlf
            checkNum $tlf

            echo -e "${nombre}\t${apellido}\t${tlf}" >> agenda.txt

            # añadir apellidos
            # añadir numeros
        else
            echo "saliendo del programa"
            exit 0
        fi
    fi
    exit 0
fi