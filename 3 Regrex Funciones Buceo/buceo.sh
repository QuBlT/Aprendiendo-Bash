#!/bin/bash

# a cuantos metros de profuncidad he llegado

# Cuanto tiempo he estado haciendo la actividad

# Que especies marinas he visto

# 20101990	50	2	 , animal1	animal2		animal3

function checkFecha {

	if [[ "$1" =~ ^[0-9]{4}(-|\/)(0[1-9]|1[0-2])(-|\/)(0[1-9]|1[0-9]|2[0-9]|3[0-1]) ]]; then
		echo "fecha Correcta"
	else
		echo "fecha incorrecta"
		exit 1
	fi
}

function checkTiempo {

	if [[ "$1" =~ ^([0-1][0-9]|2[0-4]):[0-5][0-9] ]]; then
		echo "Tiempo Correcto"
	else
		echo "Tiempo incorrecto, Formato	[hh:mm]"
		exit 1
	fi
}

function chekString {
	if [[ "$1" =~ ^[a-zA-Z]*$ ]]; then
		echo "valido"
	else
		echo "ERROR: Caracteres validos desde A a Z"
		exit 1
	fi
}

 function cheknum {
         if [[ "$1" =~ ^[0-9]*$ ]]; then
                 echo "valido"
         else
                 echo "ERROR: Solo numeros son Validos (0-9)"
                 exit 1
         fi
}

function readData {
			echo -n "Metros de profundidad buceados: "
			read metros
			cheknum $metros

			echo -n "Tiempo de buceo: "
			read tiempo
			checkTiempo $tiempo

			echo -n "¿Has descubierto algún Animal? "
			read animales
			chekString $animales
}

# si no introduces argumentos mustra el diario completo
if [ -z $1 ]; then
	cat diario
	exit 0
else
	if [ "$1" == "add" ]; then
		# que dia he buceado
		echo -n "Introduce fecha: "
		read fecha
		checkFecha $fecha
		
		linea=$(cat diario | cut -f1 | tr -d "-" | tr -d "/" | grep -n $(echo "$fecha" | tr -d "-" | tr -d "/")  | cut -d":" -f1)
		echo $linea

		if [ ! -z "$linea" ]; then
		
			echo "La ${fecha} ya existe, ¿deseas modificarla?(s/n)"

			read respuesta
		
			if [[ "$respuesta" =~ [nN] ]]; then
				echo "No"
				exit 0
			fi
		else
			linea=0
		fi

		readData 

		echo "Actualizando diario..."
		echo
		echo -e "\tDia de la inmersión:	${fecha}"
		echo -e "\tProfundidad alcanzada:	${metros} metros."
		echo -e "\tTiempo buceando:	${tiempo} horas."
		echo -e "\tAnimal/es descubierto/s:	${animales}"

		entrada="${fecha}\t${metros}\t${tiempo},\t${animales}"

		if [ "$linea" -eq 0 ]; then

			echo -e ${entrada} >> diario			
		else 
			sed  "${linea}s/.*/$entrada/" diario
		fi


	elif [ "$1" == "del" ] && [ ! -z "$2" ]; then
		# buscamos en fichero la linea y la borramos
		fechaaBorrar=$2
		sed "/$fechaaBorrar/d" diario
		echo $fechaaBorrar se ha borrado del diario correctamente
		exit 0

	fi
fi





