Bash Basics
==
###### tags: `Bash` `Sh` `Tty` `Codespace` `Scripting`


## Salidas y entradas estandar

### Stdin (0)

**Stdin** es la entrada estandar

- teclado
- fichero
- socket
- programa

### Stdout (1)

**Stdout** es la salida estandar

- pantalla
- socket
- fichero
- programa

### Sterr (2)

- pantalla
- sochet
- fichero
- programa


### Redireccionando Salidas estandar

Si queremos modificar las salidas stout y stderr lo podemos hacer usando los numeros 1 y 2.

Sacar los errores al fichero `error` y l resultado al fichero `resultado`
```
ls 2> error 1> resultado 
```
Sacar los errores y el resultado al fichero `sterrout`
```
ls &> sterrout
```

```
ls > sterrout 2>&1
```



## Modificadores

### Tubería    | 

![](https://i.imgur.com/mFy5hA2.png)

### <()

Lanzar un comando entre `< ()` provoca que se cree un fichero temporal con el contenido de dicho comando y **lo pasa como parametro**.

```
cat <(date)

Fri 22 Jan 2021 11:08:54 AM EST

```


### <<<

here string

transforma cualquier información recibida en una **cadena**

```
cat <<< <(date)
/proc/self/fd/11
```

![](https://i.imgur.com/96XOLRe.png)

### <> 
Estos modificadores sirven para redirigir las salidas y entradas estandar

Esto crea un archivo con la cadena saludo. Y sobreescribirá todo lo que haya en el archivo
```
echo "hola" > saludo
cat saludo
hola
```
```
echo "hola" > saludo
echo "adios" > saludo
cat saludo
adios
```

Si queremos añadir al final del archivo y no sobre escribirlo tendremos que utilizar `>>`.
```
echo "hola" > saludo
echo "adios" >> saludo
cat saludo
hola
adios
```

Coje el contenido de un fichero y se lo pasa a `cat` por el **stdin**

```
cat < <(date)
```

Genera un fichero temporal y lo pasa a cat como **argumento**

```
cat <(date)
```

Coje el contenido de un fichero y se lo pasa a `cat` por el **stdin** y el resultado lo guarda en un archivo final.

```
cat < <(date) > file
```

### << EOF

Mete por la entrada estandar un conjunto de de lineas. Por donde empieza y por donde acaba. Lo decide por el Nombre que le pongas despues del `<<`

```
cat << FILE

HOLA
QUE
TAL
FILE
```



## Build-ins de bash

Bash consta de un conjunto de programas integrados, tales como `echo` `kill` y bastantes más que pueden ser mostrados con el comando `compgen -b`.

https://www.cyberciti.biz/faq/linux-unix-bash-shell-list-all-builtin-commands/

## Donde se encuantra un programa

```
whereis binfile
```

```
which binfile
```

## Shebang
Es la primera linea que hay en un fichero ejecutable para indicar al systema con que binario ejecutarlo.

```
#!/bin/bash
#!/bin/env ^[a-zA-Z]+$bash
#!/bin/sh
#!/bin/python
#!/bin/ruby
```

https://excalidraw.com/#json=5394089281323008,uaxA_8HBqLtK8quZkIoEag


## Funciones

```bash=
# Definir función

## Contar archivos
function eqothings {
    echo $1
    find $1 -type f | wc -l
}


# llamar función

eqothings maretilosillo

```

## Variables Especiales
### $?

Esta variable devuelve `$?` el resultado de un proceso anterior. 

- Si devuelve `0` significa que **todo salio bien** o que ha devuelto `true`.

Por regla general significa que **si las cosas van bien** devuelve `0` ya sea un comando, una funcion un if.

- Si devuelve `1` significa que la el programa ha fallado y ha devuelto `false`.


```bash=
#!/bin/bash

string_a="hola"
string_b="adios"

echo "Son iguales?"
[$string_a  = $string_a]
echo $?
# output 1

num1=100
num2=200

[$num1 = $num2]
echo $1
# output 0

```
### $#

Numero de argumentos que se han metido

### $*

Array de argumentos introducidos

### !$

Guara el ultimo argumento que se ha introducido


### ${1:-default}

Esta variable se puede utilizar para dar un valor predeterminado al argumento 1 en el caso que no se haya introducido ninguno

```bash=
ip={1:-defalut}
```

### !!

Ejecuta el ultimo comando
```bash
ls /root
# no tienes permiso
sudo !!
```

#### Coje el parametro del ultimo comando

Permite coger diferentes parametros.
```bash
ls /opt/home/root

cd !!:1
```

## Control de Flujo
### Comparadores
| symbol | meaning | icon       |              
|--------|-------- |    ---     |     
| -lt   | lowerthan     | <     |           
|-gt    | mayor que     | >     |            
| -le   | menor o igual | <=    |
| -ge   | mayor o igual | >=    |
| -eq   | igual         | =     |
| -me   | distinto que  | !=    |
| -a    | si existe     |       |
| -z    | Si Cadena = 0 |       |
| -d    | Si directorio exite |
| -e    | si está disponible  |
| -f    | Si existe un fichero|
| -r    | Si es de lectura    |
| -w    | Si es de escritura  |
| -x    | Si es ejecutable    |

### IF

#### Simple

```bash=
if [ -x hola.txt]; then
    echo "Ejecutando fichero"
fi
```
#### Si nó

```bash=
if [ -z $1 ]: then
    echo "no argumentos"
    
else
    if [ ! -d /home/${1} ]
        echo "el directorio no existe"
    else
        echo "Creando backups de $1"
    fi
    
fi
```

#### Varias condiciones

```bash=
if [ -x hola.txt]; then
    echo "Ejecutando fichero"
elif [-x hola.txt]
    echo "El fichero no se puede ejecutar"
else
    echo "hola quetal"
fi
```

### Loop

Loop que se repite tantas veces como argumentos haya recibido la función.
```bash=
for numeros in $*; 
do
    echo $numeros
done
```

Loop que se repite un numero limitado de veces

```bash=
for numeros in {1..20}; 
do
    echo $numeros
done
```

## Aritmetica

### Let

```bash=
a=13

let a--
echo $a
```

```bash=
let c=3**3

echo $c
```

### decimales con bc
```bash=
echo "scale=30; 8.5 /2.3" | bc
```

## Comandos básicos

### Find

Busca los directorios que hay en home
```bash=
find /home -type d
```
Busca los directorios que hay en home y cuenta la cantidad de lineas que hay
```bash=
find /home -type d | wc -l
```


Busca los archivos que hay en home
```bash=
find /home -type f
```
Busca los archivos que hay en home y cuenta la cantidad de lineas que hay
```bash=
find /home -type f | wc -l
```




### xargs 

Coje una lista y ejecuta un comando por cada linea de la lista.
Donde `{}` es la linea actual en la que se encuentra.

```bash
cat list | xargs -I {} cat {}
```

```bash
cat list | xargs -I {} mov {} {}_movido
```

### Sort

Te muetra texto organizado


```bash=
sort lista
```


Organiza una lista y quita los repetidos
```bash=
sort -u
```

### tr

Coje un caracter y lo cambia por otro

```bash=
cat list | tr '\n'  ' '
```

### cd -

Entra en el ultimo directorio utilizado

`cd -`

### sed

Borra Hola
```bash=
cat file | sed -e 's/hola//g'
```
Añadir una linea al final
```bash=
sed '$a conntent' - 
```

```bash=
Borrar por arriba
sed '$1i content' -
```

Sustituir una linea concreta por otra

```bash=
sed  "$NumeroLinea/.*/$DatosaCambiar/" fichero
```

Borrar una linea

```bash=
sed "/$TextoenLalinea/d" fichero
```

### Cut

Corta el continido de cadenas de diferentes maneras.

Coger la primera fila. Delimitador predeterminado `\t`

```
cut -f1
```

Coger la segunda fila con delimitador `:`

```bash=
cut -d":" -f2
```

### egrep

Coger las lineas que acaben por `/`

```bash=
egrep "/$"
```

Coger lo que empieze por `es`

```bash=
egrep "^es"
```


Las dos anteriores juntas

```bash=
egrep "^es|/$"
```


Buscar todo lo que hay entre parentesis y contiene todas las letras mayusculas y minisculas.

```bash
grep "([A-za-z]*)"
```

Todo lo que Empieze por mayuscula y acabe en punto

```bash
grep "^[A-Z]\.$"
```

Utilizando OR en grep

```bash
grep -E "(^A|\.$)
```

Busca palabras que tengan 3 vocales seguidas

```bash
grep -E "[AEIOUaeiou]{3}" 
```

Palabras que tengan entre 16 y 20 caracteres
```bash
grepp -E "[[:alpha:]]{16,20}" archivo
```

### read

Guarda en una variable lo introducido por el teclado.

```bash=
read nombre
```

## Ejecución de Comandos

### ;

ejecutar dos comandos

```bash=
ls /etze ; echo "Comando ejecutado"
```

### &&

ejecutar dos comandos **Solo si el primero se ha ejecutado**

```bash=
ls /etze && echo "Hola"
```


## Reff

https://askubuntu.com/questions/444082/how-to-check-if-1-and-2-are-null
http://asir2.blogspot.com/p/scripts-bash-ejercicios.html
https://ryanstutorials.net/bash-scripting-tutorial/bash-loops.php

















